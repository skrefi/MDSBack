﻿using MoreSandInTheBox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MoreSandInTheBox.Controllers
{
    public class StudentController : Controller
    {
        private StudentDBContext db = new StudentDBContext();
        // GET: Student
        public ActionResult Index()
        {
            return Json(new { message = "Hello world" }, JsonRequestBehavior.AllowGet);
        }
    }
}