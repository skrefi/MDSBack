﻿using MoreSandInTheBox.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MoreSandInTheBox.Controllers
{
    public class HomeController : Controller
    {
        private StudentDBContext db = new StudentDBContext();

        public ActionResult Index()
        {
            var students = from student in db.Students
                           orderby student.Name
                           select student;

            ViewBag.Students = students;

            return View();
        }

        public ActionResult Show(int id)
        {
            Student student = db.Students.Find(id);
            ViewBag.Student = student;

            return View();
        }

        public ActionResult New(Student student)
        {
            try
            {
                db.Students.Add(student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        public ActionResult Edit(int id, Student reqStudent)
        {
            try
            {
                Student student = db.Students.Find(id);
                if (TryUpdateModel(student))
                {
                    student.Name = reqStudent.Name;
                    student.Email = reqStudent.Email;
                    student.CNP = reqStudent.CNP;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            db.Students.Remove(db.Students.Find(id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}