﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MoreSandInTheBox.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CNP { get; set; }
    }

    public class StudentDBContext : DbContext
    {
        public StudentDBContext() : base("DBConnectionString") { }
        public DbSet<Student> Students { get; set; }
    }
}